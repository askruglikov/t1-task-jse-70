package ru.t1.kruglikov.tm.api.endpoint;

import java.io.IOException;
import java.net.Socket;

public interface IEndpointClient {

    Socket connect() throws IOException;

    void disconnect() throws IOException;

    void setSocket(final Socket socket);

}
