package ru.t1.kruglikov.tm.api;

import org.jetbrains.annotations.Nullable;
import ru.t1.kruglikov.tm.event.ConsoleEvent;

public interface IListener {

    @Nullable
    String getArgument();

    @Nullable
    String getDescription();

    @Nullable
    String getName();

    void handler(ConsoleEvent consoleEvent);

}
