package ru.kruglikov.tm.api.repository;

import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.kruglikov.tm.model.Task;

import java.util.List;

@Repository
public interface ITaskRepository extends JpaRepository<Task, String> {

    void deleteAllByUserId(String userId);

    @Nullable
    List<Task> findAllByUserId(String userId);

    void deleteByIdAndUserId(String id, String userId);

    @Nullable
    Task findByIdAndUserId(String id, String userId);

}


